from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session

mod = Blueprint('posts', __name__)

@mod.route('/')
def post_list():
    posts = g.postsdb.getPosts(session['username'])
    posts_list = list(posts)
    posts = g.postsdb.getEmotions(session['username'])
    emotion_list = list(posts)
    return render_template('posts/post.html', posts=posts_list,emotions=emotion_list, date='October 20 2015')


@mod.route('/d', methods=['POST'])
def delete_post():
    old_post = request.form['old_post']
    old_emotion = request.form['old_emotion']
    g.postsdb.deletePost(old_post, old_emotion, session['username'])
    #g.postsdb.deletePostOld(old_post)
    return redirect(url_for('.post_list'))

@mod.route('/c', methods=['POST'])
def create_post():
    new_post = request.form['new_post']
    new_emotion = request.form['new_emotion']
    g.postsdb.createPost(new_post, new_emotion, session['username'])
    flash('New post created!', 'create_post_success')
    return redirect(url_for('.post_list'))
