class PostDB:
    def __init__(self, conn):
        self.conn = conn

    def getPosts(self, username):
        return self.conn.find({'username':username}, {'post':1, '_id':0})

    def createPost(self, post, emotion, username):
        self.conn.insert({'post':post, 'emotion':emotion, 'username': username})

    def deletePost(self, post, emotion, username):
        self.conn.delete_one({'post':post, 'emotion':emotion, 'username':username})

    def deletePostOld(self, post):
        self.conn.delete_one({'post':post})

    def getEmotions(self, username):
        return self.conn.find({'username':username}, {'emotion':1, '_id':0})
